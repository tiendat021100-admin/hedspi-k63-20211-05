
-Phân công công việc:

- Bài tập tuần 2 : 
 + Phạm Tiến Đạt: vẽ và đặc tả use case "Trả xe".
 + Nguyễn Đắc Nam: vẽ và đặc tả use case "Tìm kiếm bãi xe".
 + Lê trung Hiếu: vẽ và đặc tả use case "Thuê xe".
 + Ngô Xuân Trường: vẽ và đặc tả use case "Quản lý loại xe".
 + Cả nhóm cùng nhau họp và thống nhất đưa ra use case tổng quát và viết báo cáo.
- Bài tập tuần 4 : 
 + Nguyễn Đắc Nam : 
      >>Vẽ và đặc tả các màn hình của chức năng "Truy vấn thông tin bãi xe"
 + Lê Trung Hiếu : 
      >>Vẽ và đặc tả các màn hình của chức năng "Thuê xe"
 + Phạm Tiến Đạt : 
      >>Đặc tả các màn hình của chức năng "Trả xe"
      >>Tổng hợp và vẽ sơ đồ chuyển đổi màn hình của user
 + Ngô Xuân Trường : 
      >>Đặc tả các màn hình của chức năng "Quản lý thông tin xe"
      >>Vẽ sơ đồ chuyển đổi màn hình của admin
- Bài tập tuần 5 :
 + Nguyễn Đắc Nam : 
      >>thiết kế biểu đồ trình tự 
      >>biểu đồ chi tiết lớp của Use Case "Truy vấn thông tin bãi xe"
 + Lê Trung Hiếu : 
      >>thiết kế biểu đồ trình tự 
      >>biểu đồ chi tiết lớp của Use Case "Thuê xe"
 + Ngô Xuân Trường : 
     >>thiết kế biểu đồ trình tự 
     >>biểu đồ chi tiết lớp của Use Case "Quản lý thông tin xe"
 + Phạm Tiến Đạt :
     >>thiết kế biểu đồ trình tự 
     >>biểu đồ chi tiết lớp của Use Case "Trả xe"
     >>thiết kế chi tiết gói cho toàn nhóm
- Bài tập tuần 6 : 
 + Nguyễn Đắc Nam : 
     >>Code và kiểm phần chức năng "Truy vấn thông tin bãi xe"
 + Lê Trung Hiếu :
     >>Code và kiểm thử phần chức năng "Thuê xe"
 + Phạm Tiến Đạt : 
     >>Code và kiểm thử phần chức năng "Trả xe"
     >>Tổng hợp code, đặt tên và chia package
 + Ngô Xuân Trường : 
     >>Code và kiểm thử phần chức năng "Quản lý thông tin xe"
- Bài tập tuần 7 : 
 + Nguyễn Đắc Nam : 
     >>Tìm hiểu về các mẫu thiết kế
     >>Viết báo cáo cá nhân và cả nhóm
 + Lê Trung Hiếu : 
     >>Tìm hiểu các mẫu thiết kế
     >>Viết báo cáo cá nhân
 + Phạm Tiến Đạt : 
     >>Tìm hiểu nguyên lý thiết kế và mẫu thiết kế
     >>Chỉnh sửa lại code
     >>Viết báo cáo cá nhân và cả nhóm
 + Ngô Xuân Trường : 
     >>Viết báo cáo cá nhân

-Phần trăm đóng góp : 
 + Project : Phạm Tiến Đạt: 35%
	     Nguyễn Đắc Nam : 25%
	     Lê Trung Hiếu : 20%
	     Ngô Xuân Trường : 20%
 + Báo cáo và bài tập tuần : Phạm Tiến Đạt : 25%
			     Nguyễn Đắc Nam : 25%
			     Lê Trung Hiếu : 25%
			     Ngô Xuân Trường : 25%

