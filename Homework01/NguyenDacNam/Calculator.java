package hello;

import java.util.*;

public class Calculator {
	public static void main(String[] args) {
		System.out.println("Enter number 1:");
		Scanner sc = new Scanner(System.in);
		float num1 = sc.nextFloat();
		System.out.println("Enter number 2:");
		float num2 = sc.nextFloat();
		float sum = num1 + num2;
		float sub = num1 - num2;
		float mul = num1 * num2;
		float dev = num1 / num2;
		System.out.println(num1 + "+" + num2 + "="+ sum );
		System.out.println(num1 + "-" + num2 + "="+ sub );
		System.out.println(num1 + "*" + num2 + "="+ mul );
		System.out.println(num1 + "/" + num2 + "="+ dev );
	}
}
