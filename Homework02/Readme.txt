Phân chia công việc Tuần 2:

1. Phạm Tiến Đạt: vẽ và đặc tả use case "Trả xe".
2. Nguyễn Đắc Nam: vẽ và đặc tả use case "Tìm kiếm bãi xe".
3. Lê trung Hiếu: vẽ và đặc tả use case "Thuê xe".
4. Ngô Xuân Trường: vẽ và đặc tả use case "Quản lý loại xe".

Cả nhóm cùng nhau họp và thống nhất đưa ra use case tổng quát và viết báo cáo.
