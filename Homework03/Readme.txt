Phân công công việc tuần 3 :

  - Nguyễn Đắc Nam :  vẽ biểu đồ trình tự lớp phân tích và
			 biểu đồ lớp phân tích cho usecase "Truy vấn thông tin bãi xe"
				
  - Lê Trung Hiếu : vẽ biểu đồ trình tự lớp phân tích và
			 biểu đồ lớp phân tích cho usecase "Thuê xe"

  - Phạm Tiến Đạt : vẽ biểu đồ trình tự lớp phân tích và
			 biểu đồ lớp phân tích cho usecase "Trả xe"

  - Ngô Xuân Trường : vẽ biểu đồ trình tự lớp phân tích và
			 biểu đồ lớp phân tích cho usecase "Quản lý thông tin xe"