Phân công công việc tuần 5 :

  - Nguyễn Đắc Nam : + thiết kế biểu đồ trình tự 
		     + biểu đồ chi tiết lớp của Use Case "Truy vấn thông tin bãi xe"

  - Lê Trung Hiếu : + thiết kế biểu đồ trình tự 
		    + biểu đồ chi tiết lớp của Use Case "Thuê xe"

  - Ngô Xuân Trường : + thiết kế biểu đồ trình tự 
  		      + biểu đồ chi tiết lớp của Use Case "Quản lý thông tin xe"

  - Phạm Tiến Đạt : + thiết kế biểu đồ trình tự 
		    + biểu đồ chi tiết lớp của Use Case "Trả xe"
		    + thiết kế chi tiết gói cho toàn nhóm
	